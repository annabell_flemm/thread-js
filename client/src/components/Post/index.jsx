import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Button } from 'semantic-ui-react';
import ContentEditable from 'react-contenteditable';
import moment from 'moment';

import styles from './styles.module.scss';

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: props.post.body,
            isEditing: false
        };
    }

    isCurrentUser = () => this.props.userId === this.props.post.userId;

    handleContentEditable = (event) => {
        const {
            target: { value }
        } = event;
        this.setState(
            { isEditing: value !== this.props.post.body }
        );
        this.setState({ body: value });
    }

    handleUpdatePost = async () => {
        const { body } = this.state;
        const {
            post: { id }
        } = this.props;
        await this.props.updatePost(id, body);
    }

    handleDeletePost = async () => {
        const {
            post: { id }
        } = this.props;
        await this.props.deletePost(id);
    }

    render() {
        const {
            post,
            likePost,
            dislikePost,
            toggleExpandedPost,
            sharePost,
        } = this.props;
        const {
            id,
            image,
            user,
            likeCount,
            dislikeCount,
            commentCount,
            createdAt
        } = post;
        const date = moment(createdAt).fromNow();
        return (
            <Card style={{ width: '100%' }}>
                {image && <Image src={image.link} wrapped ui={false} />}
                <Card.Content>
                    <Card.Meta>
                        <span className="date">
                            posted by
                            {' '}
                            {user.username}
                            {' - '}
                            {date}
                        </span>
                    </Card.Meta>
                    <Card.Description>
                        {this.isCurrentUser() ? (
                            <ContentEditable
                                html={this.state.body}
                                className="content-editable"
                                onChange={this.handleContentEditable}
                            />
                        ) : this.state.body}
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                        <Icon name="thumbs up" />
                        {likeCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                        <Icon name="thumbs down" />
                        {dislikeCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                        <Icon name="comment" />
                        {commentCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                        <Icon name="share alternate" />
                    </Label>
                    <Button negative disabled={!this.isCurrentUser()} size="mini" floated="right" as="button" onClick={this.handleDeletePost}>Delete</Button>
                    <Button positive disabled={!this.state.isEditing} size="mini" floated="right" as="button" onClick={this.handleUpdatePost}>Save</Button>
                </Card.Content>
            </Card>
        );
    }
}

Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    userId: PropTypes.string.isRequired
};

export default Post;
